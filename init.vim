set autoindent
set shiftwidth=2
set hlsearch
set encoding=utf-8
syntax enable
set ruler
set number
set title
