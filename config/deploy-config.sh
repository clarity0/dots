#!/bin/bash

[[ $EUID -eq 0 ]] &&\
  cp doas.conf /etc/
  cp rules.v4 /etc/iptables/
  cp sshd_config /etc/ssh/
