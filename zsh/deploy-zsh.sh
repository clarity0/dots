#!/bin/bash

[[ $EUID -eq 0 ]] &&\
  cp zshenv /etc/zsh/
  cp zshrc  /etc/zsh/
  cp aliases /etc/
